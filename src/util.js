/* Sample Functions to Test */

function getCircleArea(radius){
    // area = pi x r^2

    return 3.1416 * (radius**2);
}

function checkIfPassed(score, total){
    // score / total * 100  >= 75

    return (score/total) * 100 >= 75
}

function getAverage(num1,num2,num3,num4){
    return (num1+num2+num3+num4)/4
}

function getSum(x,y){
    z = x + y
    return z
}

function getDifference(x,y){
    z = x - y
    return z
}

function div_check(x){
    if(x % 5 === 0 || x % 7 === 0){
          return true
    } else {return false}
};

function isOddOrEven(x){
    if(x % 2 === 0){
        return "even"
    } else {return "odd"}
}

function reverseString(str){
    var splitString = str.split("");
    var reverseArray = splitString.reverse(); 
    var joinArray = reverseArray.join(""); 
    return joinArray;
}

module.exports = {
    getCircleArea: getCircleArea,
    checkIfPassed: checkIfPassed,
    getAverage: getAverage,
    getSum: getSum,
    getDifference: getDifference,
    div_check: div_check,
    isOddOrEven: isOddOrEven,
    reverseString: reverseString
}

/*
Mini Activity
Create a new test suite to test cases for the getAverage function
test case 1: assert that the average of 80,82,84,86 is 83
test case 1: assert that the average of 70,80,82,84 is 79
*/

/* 
Mini Activity 2
Create a function called getSum which is able to receive 2 numbers and returns the sum of both arguments.

Create a test suite to check the function result for the following pairs of numbers:
15, 30
25, 50

Create a function called getDifference which is able to receive 2 numbers and returns the difference of both arguments.

Create a test suite to check the function result for the following pairs of numbers:
70, 40
125, 50
*/

