const { getCircleArea, checkIfPassed, getAverage, getSum, getDifference, div_check, isOddOrEven, reverseString } = require('../src/util.js');
// import the assert statements from our chai
const { expect,assert } = require('chai');

// test case - a condition whe are testing

// it(stringToExplainWhatTheTestDoes,function)

// assert is used to assert conditions for the test to pass. If the assertion fails, then the test is considered failed.

// describe() is used to create a test suite. A test suite is a group of test cases related to one another or tests the same method, data, or function.
describe('test_get_area_circle_area', ()=> {
    it('test_area_of_circle_radius_15_is_706.86',()=>{
        let area = getCircleArea(15);
        assert.equal(area,706.86);
    });
    it('test_area_of_circle_radius_300_is_282744',()=> {
        let area = getCircleArea(300);
        expect(area).to.equal(282744);
    });
});

describe('test_check_if_passed', ()=> {
    it('test_25_out_of_30_passed', ()=> {
        let isPassed = checkIfPassed(25,30)
        assert.equal(isPassed,true);
    });
    it('test_30_out_of_50_is_not_passed', ()=> {
        let isPassed = checkIfPassed(30,50)
        assert.equal(isPassed,false);
    })
});

describe('test_check_average', ()=> {
    it('test_the_average_of_80,82,84,86_is_83', ()=> {
        let average = getAverage(80,82,84,86)
        assert.equal(average,83);
    });
    it('test_the_average_of_70,80,82,84_is_79', ()=> {
        let average = getAverage(70,80,82,84)
        assert.equal(average,79);
    })
});

describe('test_get_sum', ()=> {
    it('test_sum_of_15_and_30_equal_to_45', ()=> {
        let sum = getSum(15,30);
        assert.equal(sum,45);
    });
    it('test_sum_of_25_and_50_equal_to_75', ()=> {
        let sum = getSum(25,50);
        assert.equal(sum,75);
    });
});

describe('test_get_difference', ()=> {
    it('test_difference_of_70_and_40_equal_to_30', ()=> {
        let difference = getDifference(70,40);
        assert.equal(difference,30);
    });
    it('test_difference_of_125_and_50_equal_to_75', ()=> {
        let difference = getDifference(125,50);
        assert.equal(difference,75);
    });
});

describe('test_divisibility_check', ()=> {
    it('test_if_35_is_true', () =>{
        let x = div_check(35);
        assert.equal(x,true)
    });
    it('test_if_10_is_true', () =>{
        let x = div_check(10);
        assert.equal(x,true)
    });
    it('test_if_14_is_true', () =>{
        let x = div_check(14);
        assert.equal(x,true)
    });
    it('test_if_13_is_false', () =>{
        let x = div_check(13);
        assert.equal(x,false)
    });
});

describe('test_odd_or_even', ()=> {
    it('test_if_5_is_odd', () =>{
        let x = isOddOrEven(5);
        assert.equal(x,"odd")
    });
    it('test_if_10_is_even', () =>{
        let x = isOddOrEven(10);
        assert.equal(x,"even")
    });
    it('test_if_1_is_odd', () =>{
        let x = isOddOrEven(1);
        assert.equal(x,"odd")
    });
    it('test_if_0_is_even', () =>{
        let x = isOddOrEven(0);
        assert.equal(x,"even")
    });
})

describe('test_reversing_string', ()=> {
    it('test_Rokki_reversed_is_ikkoR', () =>{
        let x = reverseString("Rokki");
        assert.equal(x,"ikkoR")
    });
    it('test_Hello_World_reversed_is_!dlroW_olleH', () =>{
        let x = reverseString("Hello World!");
        assert.equal(x,"!dlroW olleH")
    });
    it('test_12345_reversed_is_54321', () =>{
        let x = reverseString("12345");
        assert.equal(x,"54321")
    });
    it('test_test_reversed_is_tset', () =>{
        let x = reverseString("test");
        assert.equal(x,"tset")
    });
})